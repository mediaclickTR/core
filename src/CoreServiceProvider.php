<?php

namespace Mediapress\Core;

use Illuminate\Support\ServiceProvider;


class CoreServiceProvider extends ServiceProvider
{
    public function boot()
    {
        //
    }

    public function register()
    {
        //
    }
}
